import turtle
from distance_between_points import Rectangle,Point
from Circle import Circle
import math
def draw_rect(t,rect):
    '''
    draw a rect
    :param rect:
    :return:
    '''
    t.pu()
    t.goto(rect.corner.x,rect.corner.y)
    t.pd()
    t.fd(rect.width)
    t.lt(90)
    t.fd(rect.height)
    t.lt(90)
    t.fd(rect.width)
    t.lt(90)
    t.fd(rect.height)
    turtle.mainloop()
def draw_circle(t,circle):
    '''
    draw a circle
    :param circle:
    :return:
    '''
    length=2*math.pi*circle.radius
    n=int(length/3)+1
    step_length=length/n
    print(step_length)
    step_angle=360/n
    print(step_angle)
    t.pu()
    t.goto(circle.center.x,circle.center.y)
    t.pd()
    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)
    turtle.mainloop()

def main():
    circle = Circle()
    circle.center = Point()
    circle.center.x = 150
    circle.center.y = 100
    circle.radius = 75

    rect = Rectangle()
    rect.corner = Point()
    rect.corner.x = 50
    rect.corner.y = 50
    rect.width = 100
    rect.height = 200
    t=turtle.Turtle()
    # draw_circle(t,circle)
    draw_rect(t,rect)
if __name__=='__main__':
    main()